import React from 'react';
import './App.css';

import Home from './pages/Home';
import Rooms from './pages/Rooms';
import SingleRoom from './pages/SingleRoom';
import Error from './pages/Error';

import { Route, Switch } from 'react-router-dom';

function App() {
  return (
    <div>
      <Route exact path="/" exact component={Home} />
      <Route exact path="/rooms" exact component={Rooms} />
      <Route exact path="/single-room" exact component={SingleRoom} />
    </div>
  );
}

export default App;
